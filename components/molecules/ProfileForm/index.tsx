

export const Form = () => {
  const onHandleForm = (e) => {
    e.preventDefault();
  };

  return (
    <form>
      <label htmlFor="name"></label>
      <input id="name" type="text" />

      <label htmlFor="name"></label>
      <input id="phoneNumber" type="text" />

      <label htmlFor="name"></label>
      <input id="birthday" type="text" />

      <label htmlFor="name"></label>
      <input id="gender" type="text" />

      <button onClick={(e) => onHandleForm(e)}>Submit Profile</button>
    </form>
  );
};
